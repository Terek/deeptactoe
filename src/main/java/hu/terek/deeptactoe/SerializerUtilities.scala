package hu.terek.deeptactoe;

import java.lang.reflect.Type

import com.google.gson._
import com.google.gson.stream.{JsonReader, JsonWriter}

import scala.reflect._

object SerializerUtilities {

	class MarkSerializer extends TypeAdapter[Mark]{
    override def read(jsonReader: JsonReader): Mark = Mark.fromString( jsonReader.nextString() )
    override def write(jsonWriter: JsonWriter, t: Mark): Unit = jsonWriter.value(t.toString)
  }


	val gson:Gson  = new GsonBuilder()
			.disableHtmlEscaping()
			.setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
			.registerTypeAdapter(classOf[Mark], new MarkSerializer)
			.create()
	
	
	def toJson[T](obj:T) : String = {
		gson.toJson(obj)
	}

	def fromJson[T] (json:String, clazz:Class[T]) : T = {
		return gson.fromJson( json, clazz );
	}
	
}
