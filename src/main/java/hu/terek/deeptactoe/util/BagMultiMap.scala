package hu.terek.deeptactoe.util

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.Map

trait BagMultiMap[A, B] extends Map[A, ArrayBuffer[B]] {

	protected def makeList : ArrayBuffer[B] = new ArrayBuffer[B]

  def add(key: A, value: B): this.type = {
    get(key) match {
      case None =>
        val seq = makeList
        seq += value
        this(key) = seq
      case Some(seq) =>
        seq += value
    }
    this
  }

  def remove(key: A, value: B): this.type = {
    get(key) match {
      case None =>
        case Some(seq) =>
          seq -= value
          if (seq.isEmpty) this -= key
    }
    this
  }

  def entryExists(key: A, p: B => Boolean): Boolean = get(key) match {
    case None => false
    case Some(seq) => seq exists p
  }
}
