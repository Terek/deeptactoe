package hu.terek.deeptactoe.util

import hu.terek.deeptactoe.Play

import scala.collection.immutable.TreeMap
import scala.util.Random


object AIResult {

  val random = new Random();


}



class AIResult(val treeMap:TreeMap[Double, Play]) {

  def getGaussianDistributedBestResult() : Play = {
    val index:Int = Math.floor(AIResult.random.nextGaussian() * 9).intValue()
    treeMap.values.drop(index).head
  }

  def getBestResult(): Play ={
    treeMap.last._2
  }

}
