package hu.terek.deeptactoe;

import hu.terek.deeptactoe.util.AIResult

import scala.collection.immutable.List
import scala.Double

object TicTacToeAI {

	val REWARD_FOR_WIN:Double = 1;
	val PUNISHMENT_FOR_LOSS:Double = - 0.5.toDouble;
	val PUNISHMENT_FOR_WRONG_MOVE:Double = -1;
	val REWARD_FOR_DRAW:Double = 0;
	
}

trait TicTacToeAI {

	def getPlay(board:TicTacToeBoard, player:Mark) : AIResult;

	def learn(board:TicTacToeBoard, play:Play, result:Double) { learn( List(new BoardTrainingData(board, play, result))) }

	def learn(learningData : List[BoardTrainingData]);

	def train(learningData : List[TrainingData]);

	def forget();

	def saveAI(fileName:String) {}
	
	def loadAI(fileName:String) {}
	
}
