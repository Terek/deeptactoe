package hu.terek.deeptactoe;

class Play(val x:Int, val y:Int, val player:Mark) extends Equals {

	def mirrorX() = {
		new Play(2-x, y, player)
	}

	def transpose() = {
		new Play(y, x, player)
	}
	
	def rotate() = {
		transpose mirrorX
	}
	
	
	def getIndex() : Int = {
	  TicTacToeBoard.getPositionFromLocation(x, y)
	}
	
	override def toString():String = {
	  "Play [x=" + x + ", y=" + y + ", player=" + player + "]"
	}
	
  def canEqual(other: Any) = {
	  other.isInstanceOf[Play]
	}

  override def equals(other: Any) = {
	  other match {
	    case that: hu.terek.deeptactoe.Play => that.canEqual(Play.this) && x == that.x && y == that.y && player == that.player
	    case _ => false
	  }
	}

  override def hashCode() = {
	  val prime = 41
	  prime * (prime * (prime + x.hashCode) + y.hashCode) + player.hashCode
	}
	
	
	
}
