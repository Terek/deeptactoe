package hu.terek.deeptactoe;



object Mark {

  case object X extends Mark { val value = 1 ; val other:Mark = O }
  case object O extends Mark{ val value = -1 ; val other:Mark = X }
  case object Empty extends Mark{ val value = 0 ; val other:Mark = X }

  lazy val values = List(X, O, Empty)

  lazy val valuesMap = values.map( item => (item.toString, item) ).toMap

  def fromString( name : String) : Mark = valuesMap(name)

}

sealed trait Mark {
  def value:Int
  def other:Mark
}


