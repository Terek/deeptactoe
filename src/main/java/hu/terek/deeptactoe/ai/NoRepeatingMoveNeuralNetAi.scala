package hu.terek.deeptactoe.ai;

import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.factory.Nd4j
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import hu.terek.deeptactoe.Mark
import hu.terek.deeptactoe.Play
import hu.terek.deeptactoe.TicTacToeBoard
import hu.terek.deeptactoe.TicTacToeBoard._
import hu.terek.deeptactoe.util.AIResult

import scala.collection.immutable.TreeMap

class NoRepeatingMoveNeuralNetAi extends NeuralNetAI {

	private val logger = LoggerFactory.getLogger(classOf[NoRepeatingMoveNeuralNetAi]);

	override def getPlay(board:TicTacToeBoard, player:Mark) : AIResult = {

        val tuples : IndexedSeq[Tuple2[Double, Play]] = for(index <- 0 to 9) yield {         
            val i = getColumn(index)
            val j = getRow(index)

        		val play : Play = new Play(i,j,player);

        		val featureList = getFeatureList(board, play);
        		
        		val features : INDArray = Nd4j.create(getFeatureArray(board, play));
        		val scoreArray : INDArray = model.output(features ,false);
        		
        		val score : Double = scoreArray.getDouble(0);

        		(score, play)
        }
        
        val treeMap : TreeMap[Double, Play] = TreeMap[Double, Play]() ++ tuples 

				new AIResult(treeMap)
	}
	
	
}
