package hu.terek.deeptactoe.ai;

import hu.terek.deeptactoe.TrainingData
import hu.terek.deeptactoe.BoardTrainingData
import hu.terek.deeptactoe.Mark
import hu.terek.deeptactoe.Play
import hu.terek.deeptactoe.TicTacToeAI
import hu.terek.deeptactoe.TicTacToeBoard
import hu.terek.deeptactoe.TicTacToeBoard._
import hu.terek.deeptactoe.util.AIResult

import scala.collection.immutable.TreeMap
import scala.util.Random

class DumbAI extends TicTacToeAI {

	val random = new Random();
	
	override def getPlay(board:TicTacToeBoard, player:Mark) : AIResult = {
	  val validIndices = 0 until 9 filter (index => board.get(index) == Mark.Empty)
	  
	  val randomValidIndex = random.shuffle(validIndices).head
	  
	  val resultPlay = new Play( getColumn(randomValidIndex),  getRow(randomValidIndex), player )

		val doubleToPlay : TreeMap[Double, Play] = new TreeMap[Double, Play]() ++ List[(Double, Play)]((0, resultPlay))

		new AIResult( doubleToPlay )

	}

  def getTrainingData(): List[TrainingData] = {
    return List()
	}

  def learn(learningData: List[BoardTrainingData]) { }
  def train(learningData: List[TrainingData]) { }

  def forget(): Unit = { }

}
