package hu.terek.deeptactoe.ai;

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import hu.terek.deeptactoe.Mark
import hu.terek.deeptactoe.Play
import hu.terek.deeptactoe.TicTacToeAI
import hu.terek.deeptactoe.TicTacToeBoard
import hu.terek.deeptactoe.TicTacToeBoard._
import hu.terek.deeptactoe.TrainingData

import scala.collection.mutable.MultiMap
import scala.collection.mutable.HashMap
import scala.collection.immutable.TreeMap
import scala.collection.immutable.SortedMap
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Set
import hu.terek.deeptactoe.util.{AIResult, BagMultiMap}

import scala.collection.mutable.ArrayBuffer
import hu.terek.deeptactoe.BoardTrainingData

class CachingAI extends LearningAI {

	val logger = LoggerFactory.getLogger(classOf[CachingAI]);

	type Features = Array[Double]
	
	val scoresForTrainingData : BagMultiMap[Features, Double] = new HashMap[Features, ArrayBuffer[Double]] with BagMultiMap[Features, Double]()

	def getPlay(board:TicTacToeBoard, player:Mark) : AIResult = {
		
        val tuples : IndexedSeq[Tuple2[Double, Play]] = for(index <- 0 to 9) yield {         
            val i = getColumn(index)
            val j = getRow(index)
            
        		val play : Play = new Play(i,j,player);

        		val featureList = getFeatureList(board, play);
        		
        		val scores = scoresForTrainingData(featureList)

        		if( !scores.isEmpty ){
          		val avg : Double = scores.sum / scores.size
          		(avg, play)
        		}
        		else{  
        		  (0d, play)
        		}
        }
        
        val treeMap : TreeMap[Double, Play] = TreeMap[Double, Play]() ++ tuples 

				new AIResult(treeMap)
	}

	def train(learningData : List[TrainingData]) {
	  for( data <- learningData ) scoresForTrainingData.add(data.features, data.score)
	}

  def forget(): Unit = {
	  scoresForTrainingData.clear
	}

}
