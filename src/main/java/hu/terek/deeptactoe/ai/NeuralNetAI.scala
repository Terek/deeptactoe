package hu.terek.deeptactoe.ai;

import java.io.File
import java.io.IOException

import org.deeplearning4j.nn.api.OptimizationAlgorithm
import org.deeplearning4j.nn.conf.MultiLayerConfiguration
import org.deeplearning4j.nn.conf.NeuralNetConfiguration
import org.deeplearning4j.nn.conf.Updater
import org.deeplearning4j.nn.conf.layers.DenseLayer
import org.deeplearning4j.nn.conf.layers.OutputLayer
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork
import org.deeplearning4j.nn.weights.WeightInit
import org.deeplearning4j.optimize.listeners.ScoreIterationListener
import org.deeplearning4j.util.ModelSerializer
import org.nd4j.linalg.api.ndarray.INDArray
import org.nd4j.linalg.dataset.DataSet
import org.nd4j.linalg.factory.Nd4j
import org.nd4j.linalg.lossfunctions.LossFunctions
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import hu.terek.deeptactoe.Mark
import hu.terek.deeptactoe.Play
import hu.terek.deeptactoe.TicTacToeAI
import hu.terek.deeptactoe.TicTacToeBoard
import hu.terek.deeptactoe.TicTacToeBoard._
import hu.terek.deeptactoe.TrainingData
import hu.terek.deeptactoe.util.AIResult

import scala.collection.immutable.TreeMap

class NeuralNetAI extends LearningAI {

	private val logger = LoggerFactory.getLogger(classOf[NoRepeatingMoveNeuralNetAi]);

	var model : MultiLayerNetwork = createNetwork();

  def forget(): Unit = {
	  model = createNetwork();
	}
	
	def  createNetwork() : MultiLayerNetwork =  {
		    val seed : Int = 123;
        val learningRate : Double = 0.005;

        val numInputs : Int = 18;
        val numOutputs : Int = 1;
        val numHiddenNodes : Int = 18;
        val numIterations : Int = 25;

		    val conf : MultiLayerConfiguration = new NeuralNetConfiguration.Builder()
//                .seed(seed)
                .iterations(numIterations)
                //.regularization(true).l1(0.01).l2(0.01)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .learningRate(learningRate)
                .updater(Updater.NESTEROVS)
                .momentum(0.09)
                .list()
                .layer(0, new DenseLayer.Builder().nIn(numInputs).nOut(numHiddenNodes)
                        .weightInit(WeightInit.XAVIER)
                        .activation("tanh")
                        .build())
                .layer(1, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                		.weightInit(WeightInit.XAVIER)
                		.activation("relu")
                		.build())
                .layer(2, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
                		.weightInit(WeightInit.XAVIER)
                		.activation("relu")
                		.build())
								.layer(3, new DenseLayer.Builder().nIn(numHiddenNodes).nOut(numHiddenNodes)
									.weightInit(WeightInit.XAVIER)
									.activation("relu")
									.build())
                .layer(4, new OutputLayer.Builder()
                        .weightInit(WeightInit.XAVIER)
                        .activation("identity")
                        .nIn(numHiddenNodes)
                        .lossFunction(LossFunctions.LossFunction.MSE)
                        .nOut(numOutputs).build())
                .pretrain(false)
                .backprop(true)
                .build();


        val multiLayerNetwork : MultiLayerNetwork = new MultiLayerNetwork(conf);
        multiLayerNetwork.init();
        multiLayerNetwork.setListeners(new ScoreIterationListener(numIterations / 4));  //Print score every 10 parameter updates
    		return multiLayerNetwork;
	}
	
		override def getPlay(board:TicTacToeBoard, player:Mark) : AIResult = {

        val tuples : IndexedSeq[Tuple2[Double, Play]] = for(index <- 0 until 9) yield {
            val i = getColumn(index)
            val j = getRow(index)

        		val play : Play = new Play(i,j,player);

        		val score : Double = getPlayScore(board, play);
        		
        		(score, play)
        }
        
        val treeMap : TreeMap[Double, Play] = TreeMap[Double, Play]() ++ tuples 


				new AIResult( treeMap )

	}

	def getPlayScore( board : TicTacToeBoard, play : Play ) : Double =  {
		val features : INDArray = Nd4j.create(getFeatureArray(board, play));
		
		val scoreArray : INDArray = model.output(features ,false);
		
		return scoreArray.getDouble(0);
	}

	def train(learningData : List[TrainingData]) {
        // model = createNetwork();

		if( learningData.size == 0) return;
		
		val features : INDArray = Nd4j.zeros(learningData.size, 18);
		val labels : INDArray = Nd4j.zeros(learningData.size, 1);
		
		for( (dataItem, index) <- learningData.zipWithIndex){
			
        labels.putScalar( Array(index, 0), dataItem.score);
        
        for(j <- 0 until dataItem.features.size){
        	features.putScalar(Array(index,j), dataItem.features(j));
        }
		}

    val ds : DataSet = new DataSet(features, labels);

		// logger.debug("training for features: " + features + " - " + labels);
		
    model.fit(ds);
	}

	override def saveAI(fileName:String) {

		println("saving model to : " + fileName)

		try {
			val locationToSave : File = new File(fileName);
			ModelSerializer.writeModel(model, locationToSave , false);
		} catch {
		  case e : IOException => throw new RuntimeException(e);
		}

		println("modell saved to : " + fileName)

	}

	override def loadAI(fileName:String) {
		try {
			val locationToLoad : File = new File(fileName);
			model = ModelSerializer.restoreMultiLayerNetwork(locationToLoad);
		} catch {
		  case e : IOException => throw new RuntimeException(e);
		}
	}
	
		
	
}
