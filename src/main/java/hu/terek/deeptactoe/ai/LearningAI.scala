package hu.terek.deeptactoe.ai;

import hu.terek.deeptactoe.BoardTrainingData;
import hu.terek.deeptactoe.Play;
import hu.terek.deeptactoe.TicTacToeAI;
import hu.terek.deeptactoe.TicTacToeBoard;
import hu.terek.deeptactoe.TrainingData;
import scala.util.Random
import scala.collection.immutable.TreeMap
import hu.terek.deeptactoe.Mark
import scala.collection.immutable.Nil

abstract class LearningAI extends TicTacToeAI {


	override def learn(learningData:List[BoardTrainingData] ) {
	  val trainingData = learningData.map( item => new TrainingData(getFeatureList(item.board, item.play), item.score ))
		train(trainingData);
	}
	
	def getFeatureList( board:TicTacToeBoard, play:Play ) : Array[Double] = {
		getFeatureArray(board, play)
	}
	
	
	def getFeatureArray( board:TicTacToeBoard, play:Play ) : Array[Double] = {

	  def getInput( mark:Mark ) : Double = {
	    if(mark == Mark.Empty) 0	      
	    else if(mark == play.player) 1
	    else return -1	    
	  }
	  
	  val stateFeatures : Array[Double] = board.state.map( item => getInput(item)).toArray
	  val playFeatures : Array[Double] = Array.fill(9)(0d).updated(play.getIndex(), 1d)

		return stateFeatures ++ playFeatures
	}

	def train( trainingData:List[TrainingData]);
	
}