package hu.terek.deeptactoe;

class TrainingData(val features:Array[Double], val score:Double) {

	override def hashCode() : Int = {
		val prime = 31;
		var result = 1;
		result = prime * result + features.hashCode;
		var temp = java.lang.Double.doubleToLongBits(score);
		temp = (temp ^ (temp >>> 32));
		result = prime * result + temp.toInt;
		return result;
	}

  override def equals(obj:Any) : Boolean = {
		if (this == obj)
			true;
		else if (obj == null)
			false;
		else if (getClass() != obj.getClass())
			false;
		else {
  		val other = obj.asInstanceOf[TrainingData]
  		features == other.features && score == other.score
		}
	}
	
}
