package hu.terek.deeptactoe;

import hu.terek.deeptactoe.Mark._;
import hu.terek.deeptactoe.TicTacToeGame._;
import hu.terek.deeptactoe.SerializerUtilities._;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;
import scala.collection.immutable.List
import scala.collection.immutable.Set


object TicTacToeGame {

  def setUpGame(board:TicTacToeBoard) : TicTacToeGame = {
    new GameInProgress(board, null, null)
  }
  def startNewGame() : TicTacToeGame = {
    new GameInProgress(new TicTacToeBoard, null, null)
  }

}

case class GameInProgress(board:TicTacToeBoard, lastPlay:Play, history:History ) extends TicTacToeGame {
	def winningMoves = Set()
	def gameOver = false
	def winner = Empty

	def play( play : Play ) : TicTacToeGame = {

		val newHistoryItem:History = new History(this, play)

		logger.debug("Playing " + play);

		if( board.get(play.x,play.y) != Empty ){
			logger.debug("Already played " + play);
			GameOver(board, play, play.player.other, newHistoryItem, Set(play));
		}
		else{
			val newBoard = board.set(play.x, play.y, play.player);

			val winnerPlays = newBoard.getWinnerPlays;

			if( !winnerPlays.isEmpty ){
				logger.debug("Game won by " + play.player);
				return GameOver(newBoard, play, play.player, newHistoryItem, winnerPlays);
			}
			else if ( newBoard.isFull ){
				logger.debug("Board full");
				return GameOver(newBoard, play, Empty, newHistoryItem, Set[Play]());
			}
			else{
				return GameInProgress(newBoard, play, newHistoryItem);
			}
		}
	}

}

case class GameOver( board:TicTacToeBoard, lastPlay:Play, winner:Mark , history:History, winningMoves:Set[Play] ) extends TicTacToeGame {
	def gameOver = true

	def play( play : Play ) : TicTacToeGame = this

}


abstract class TicTacToeGame {

  def board:TicTacToeBoard
  def lastPlay:Play
  def gameOver:Boolean
  def winner:Mark
  def winningMoves:Set[Play]
  def history:History

	val logger = LoggerFactory.getLogger(classOf[TicTacToeGame]);



	def play( play : Play ) : TicTacToeGame


	def getNextPlayer() : Mark = {
		if(lastPlay == null){
			X
		}
		else{
			lastPlay.player.other
		}
	}
	
	override def toString() : String = return gson.toJson(this)
	
	
}
