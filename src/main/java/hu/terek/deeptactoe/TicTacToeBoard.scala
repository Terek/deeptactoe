package hu.terek.deeptactoe

import hu.terek.deeptactoe.Mark._
import hu.terek.deeptactoe.SerializerUtilities.gson
import hu.terek.deeptactoe.TicTacToeBoard._

import scala.collection.immutable.{IndexedSeq, List, Set}

object TicTacToeBoard {
  
	val size = 3
	val sizeRange = (0 until size) toSet
  
	def getEmptyState: Array[Mark] = Array.fill(9)(Empty)

	def getPositionFromLocation(i:Int, j:Int) : Int = i + 3*j

	def getRow(index:Int) : Int = index / 3
	def getColumn(index:Int) : Int = index % 3

	def getWinFieldCombinations() : Set[Set[Int]] = {
				
		val diagonal1 = for ( i <- sizeRange) yield { getPositionFromLocation(i, i) }
		val diagonal2 = for ( i <- sizeRange) yield { getPositionFromLocation(i, 2-i) }

		val columns = for (column <- sizeRange ) yield {
		  for(row <- sizeRange) yield {
		    getPositionFromLocation(row, column)
		  }
		}

		val rows = for (row <- sizeRange ) yield {
		  for(column <- sizeRange) yield {
		    getPositionFromLocation(row, column)
		  }
		}

		columns ++ rows + diagonal1 + diagonal2
	}
	
	def isAllTheSameAndNotEmpty( plays:Set[Play] ) : Boolean = {
	  val allIsX = plays.forall { _.player == X }
	  val allIsO = plays.forall { _.player == O }
	  
	  allIsX || allIsO
	}
	
	def getAllPlays( player:Mark ) : Set[Play] = {
		for(i <- sizeRange; j <- sizeRange) yield {
				new Play(i, j, player);
		}
	}
  
}

case class TicTacToeBoard ( val state:Array[Mark] ) {

  def this() {
    this( getEmptyState )
  }
  
  def this( board:TicTacToeBoard ){
    this ( board.state )
  }

  def this( state : Seq[Mark] ){
    this ( state.toArray )
  }

	def set(i:Int, j:Int, mark:Mark):TicTacToeBoard = {
		val newState = state.updated(getPositionFromLocation(i, j), mark)
		new TicTacToeBoard( newState )
	}

	def get(i:Int, j:Int) : Mark = state(getPositionFromLocation(i, j))
	def get(index:Int) : Mark = state(index)


	def getWinPlayCombinations() : Set[Set[Play]] =  {
		getWinFieldCombinations().map(winCombination => winCombination.map { field => getPlay(field) })
	}
	
	def getPossibleWinnerPlays(player:Mark) : Set[Play] = {
		
	  def hasTwoOfTheSameKind( fields : Set[Play] ) = {
	    fields.count { item => item.player == player } == 2
	  }

		def findEmpty( plays : Set[Play]) : Option[Play]  = plays.find(item => item.player == Empty)

		getWinPlayCombinations()
	    .filter ( hasTwoOfTheSameKind )
	    .map( findEmpty )
	    .flatten
			.map( play => new Play(play.x, play.y, player))
	}
	
	def getWinnerPlays() : Set[Play] = {
	  getWinPlayCombinations().find ( isAllTheSameAndNotEmpty ).getOrElse(Set())
	}
	
	def getPlay(x:Int, y:Int) : Play = new Play(x, y, get(x, y))

	def getPlay(i:Int) : Play = new Play(getColumn(i), getRow(i), state(i))


	def mirrorX() : TicTacToeBoard = {
	  val newBoard = for(index <- 0 until size * size) yield {
	    val newI = 2 - getColumn(index)
	    val newJ = getRow(index)
	    state( getPositionFromLocation(newI, newJ) )
	  }
	    
		new TicTacToeBoard(newBoard)
	}
	
	def transpose() : TicTacToeBoard = {

	  val newBoard = for(index <- 0 until size * size) yield {
	    val newI = getRow(index)
	    val newJ = getColumn(index)
	    state( getPositionFromLocation(newI, newJ) )
	  }
	    
		new TicTacToeBoard(newBoard)
	}
	
	
	def rotate() : TicTacToeBoard = {
		transpose mirrorX
	}

	override def toString() : String = return gson.toJson(this)

	def isFull() : Boolean = state.forall(item => item != Empty)

	def getAllValidPlays(player : Mark) : Set[Play] = {
		getAllPlays(player).filter( item => this.get(item.x, item.y) == Mark.Empty)
	}

	def canEqual(other: Any): Boolean = other.isInstanceOf[TicTacToeBoard]

	override def equals(other: Any): Boolean = other match {
		case that: TicTacToeBoard =>
			(that canEqual this) &&
				state.deep == that.state.deep
		case _ => false
	}

	override def hashCode(): Int = {
		state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
