package hu.terek.deeptactoe;

class PlayFeatures(val board:TicTacToeBoard , val play:Play) {

	override def toString() : String = {
		"PlayFeatures [board=" + board + ", play=" + play + "]";
	}

	def canEqual(other: Any): Boolean = other.isInstanceOf[PlayFeatures]

	override def equals(other: Any): Boolean = other match {
		case that: PlayFeatures =>
			(that canEqual this) &&
				board == that.board &&
				play == that.play
		case _ => false
	}

	override def hashCode(): Int = {
		val state = Seq(board, play)
		state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}