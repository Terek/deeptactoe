package hu.terek.deeptactoe.gui;

import java.awt.BasicStroke;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.terek.deeptactoe.Mark;
import hu.terek.deeptactoe.Play;
import hu.terek.deeptactoe.TicTacToeAI;
import hu.terek.deeptactoe.TicTacToeGame;
import hu.terek.deeptactoe.ai.DumbAI;
import hu.terek.deeptactoe.ai.NeuralNetAI;
import hu.terek.deeptactoe.ai.NoRepeatingMoveNeuralNetAi;
import java.awt.event.ActionListener
import java.awt.event.ActionEvent

object DeepTacToeGUI {

  val logger = LoggerFactory.getLogger(classOf[TicTacToeGame]);

  def main(args: Array[String]): Unit = {
    logger.debug("Starting up");
    new DeepTacToeGUI().initWindow();
  }

}


class DeepTacToeGUI extends MouseListener {

  
  implicit def toActionListener(f: ActionEvent => Unit) = new ActionListener {
    def actionPerformed(e: ActionEvent) { f(e) }
  }
  

	val WIDTH = 900;
	val HEIGHT = 900;



	val matchMaker:MatchMaker = new MatchMaker(new NeuralNetAI()) ;

  val frame:JFrame = new JFrame("DeepTacToe");

	
	def initWindow() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		val panel = new JPanel() {
			override def paintComponent( g : Graphics) {
				drawState( g.asInstanceOf[Graphics2D] );
			}
		};
		
		panel.addMouseListener(this);

		panel.setSize(WIDTH, HEIGHT);

		frame.getContentPane().setLayout(new BorderLayout());

		val buttonPanel = new JPanel();

		val backButton = new JButton("Back");
		val resetButton = new JButton("Reset");

		val makeAMoveButton = new JButton("Make a move");
		
		val forgetButton = new JButton("Forget");
		val loadButton = new JButton("Load knowledge from disk");

		val learnButton = new JButton("Learn a lot of games");

		
		
		buttonPanel.add(backButton);
		buttonPanel.add(resetButton);
		buttonPanel.add(learnButton);

		// buttonPanel.add(forgetButton);
		buttonPanel.add(loadButton);
		buttonPanel.add(makeAMoveButton);

		backButton.addActionListener{ event:ActionEvent => back() };
		makeAMoveButton.addActionListener{ event:ActionEvent => makeAMove()};
		resetButton.addActionListener{ event:ActionEvent => resetGame()};
		forgetButton.addActionListener{ event:ActionEvent => forget()};
		loadButton.addActionListener{ event:ActionEvent => tryLoadAi()};

		frame.getContentPane().add(buttonPanel, BorderLayout.NORTH);
		frame.getContentPane().add(panel);
		frame.pack();
		frame.setVisible(true);
		frame.setSize(WIDTH, HEIGHT + 70);
	}
	
	def makeAMove() {
		matchMaker.aiPlay();
		frame.repaint();
	}

	def back() {
		matchMaker.back();
		frame.repaint();
	}

	def tryLoadAi() {
		matchMaker.loadAi("savedNET.zip");
	}

	def forget() {
		matchMaker.forget();
	}
	
	def resetGame() {
		matchMaker.resetGame();
		frame.repaint();
	}
	
	def play( x:Int,  y:Int ) {
		matchMaker.play(x, y);

		matchMaker.aiPlay();
		
		frame.repaint();
	}


	def drawState( graphics: Graphics2D) {
		graphics.setBackground(Color.white);
		graphics.clearRect(0, 0, WIDTH, HEIGHT);

		val xOffset = WIDTH / 3;
		val yOffset = HEIGHT / 3;
		
		graphics.setColor(Color.gray);
		graphics.setStroke(new BasicStroke(10));
		for(i <- 1 until 3){
			graphics.drawLine(xOffset * i, 0, xOffset * i, HEIGHT);
			graphics.drawLine(0, yOffset * i, WIDTH, yOffset * i);
		}

		for(i <- 0 until 3){
			for(j <- 0 until 3){
				drawItem(graphics, i, j, Color.BLUE);
			}
		}
		
		for(play <- matchMaker.game.winningMoves){
			drawMark(graphics, play.x, play.y,play.player, Color.RED);
		}
		
		if(matchMaker.game.gameOver){
			graphics.setColor(Color.BLACK);
			val font = new Font("Garamond", Font.BOLD, 20);
			graphics.setFont(font);
			graphics.drawString("Game Over, winner is : " + matchMaker.game.winner , 100, 100);
			
		}
	}

	def drawItem( graphics:Graphics2D, i:Int, j:Int, color:Color) {
		val mark = matchMaker.game.board.get(i, j);

		drawMark(graphics, i, j, mark, color);
	}

	def drawMark(graphics:Graphics2D, i:Int, j:Int, mark:Mark, color:Color) {
		if(mark == Mark.O){
			drawO(graphics, i, j, color);
		}
		else if(mark == Mark.X){
			drawX(graphics, i, j, color);
		}
	}

	def drawX(graphics:Graphics2D, i:Int, j:Int, color:Color) {
		graphics.setColor(color);

		val xOffset = WIDTH / 3;
		val yOffset = HEIGHT / 3;
		
		val yDiff = yOffset / 5;
		val xDiff = xOffset / 5;
		graphics.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		graphics.drawLine(i*xOffset + xDiff, j*yOffset + yDiff , (i+1)*xOffset - xDiff, (j+1) * yOffset - yDiff);
		graphics.drawLine(i*xOffset + xDiff, (j+1)*yOffset - yDiff , (i+1)*xOffset - xDiff, j * yOffset + yDiff);
	}

	def drawO(graphics:Graphics2D, i:Int, j:Int, color:Color) {
		graphics.setColor(color);

		val xOffset = WIDTH / 3;
		val yOffset = HEIGHT / 3;
		
		val yDiff = yOffset / 5;
		val xDiff = xOffset / 5;
		graphics.setStroke(new BasicStroke(10, BasicStroke.CAP_ROUND, BasicStroke.JOIN_ROUND));
		graphics.drawOval(i*xOffset + xDiff, j*yOffset + yDiff , xOffset * 3 / 5, yOffset * 3 / 5);
	}

	@Override
	def mouseClicked( event:MouseEvent ) {
		val xOffset = WIDTH / 3;
		val yOffset = HEIGHT / 3;
		

		val x = event.getX() / xOffset;
		val y = event.getY() / yOffset;
		
		play(x, y);
		
		frame.repaint();
	}

	override def mouseEntered( event:MouseEvent ) {}

	override def mouseExited( event:MouseEvent ) {}

	override def mousePressed( event:MouseEvent ) {}

	override def mouseReleased( event:MouseEvent ) {}
	
}
