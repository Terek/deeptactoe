package hu.terek.deeptactoe.gui;

import hu.terek.deeptactoe.TicTacToeAI._;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hu.terek.deeptactoe.BoardTrainingData;
import hu.terek.deeptactoe.History;
import hu.terek.deeptactoe.Mark;
import hu.terek.deeptactoe.Play;
import hu.terek.deeptactoe.TicTacToeAI;
import hu.terek.deeptactoe.TicTacToeBoard;
import hu.terek.deeptactoe.TicTacToeGame;
import hu.terek.deeptactoe.TrainingData;
import scala.collection.immutable.List
import scala.io.Source

object MatchMaker {
  
  
	def createBoardTrainingData( trainingGames : List[TicTacToeGame]) : List[BoardTrainingData] =  {
	  trainingGames.flatMap( item => learnGame(item) )
	}
	
	def learnGame(game:TicTacToeGame) : Set[BoardTrainingData] = {
		 // logger.debug("learning game: " + game);

		
		if(game.board.isFull()){
			// logger.debug("learning from draw...");
			learnFromGame(game, Mark.O, REWARD_FOR_DRAW) ++ learnFromGame(game, Mark.X, REWARD_FOR_DRAW)
		}
		else if(game.winningMoves.size == 1){
			// wrong move
			trainCombinations(game.history.game.board, game.history.play, PUNISHMENT_FOR_WRONG_MOVE)
			
			// trainingData.addAll( learnFromGame(game.getLastHistory().getGame(), Mark.O, 0));
			// trainingData.addAll( learnFromGame(game.getLastHistory().getGame(), Mark.X, 0));
		}
		else{
			// someone won
			val winner = game.winner;

			learnFromGame(game, winner, REWARD_FOR_WIN) ++ learnFromGame(game, winner.other, PUNISHMENT_FOR_LOSS);
		}
	}
	
		def trainCombinations( board:TicTacToeBoard, play:Play, score:Double) : Set[BoardTrainingData] = {

	    Set(new BoardTrainingData(board, play, score))
	  
    	  // 		Set<BoardTrainingData> trainingData = new HashSet<>();
    		
    //   logger.debug("learning that " + play + " when " + board + " is " + score);
    
    //		trainingData.add( new BoardTrainingData(board, play, score) );
    //		trainingData.add( new BoardTrainingData(board.mirrorX(), play.mirrorX(), score) );
    //
    //		board = board.rotate();
    //
    //		trainingData.add( new BoardTrainingData(board, play, score) );
    //		trainingData.add( new BoardTrainingData(board.mirrorX(), play.mirrorX(), score) );
    //		
    //		board = board.rotate();
    //
    //		trainingData.add( new BoardTrainingData(board, play, score) );
    //		trainingData.add( new BoardTrainingData(board.mirrorX(), play.mirrorX(), score) );
    //		
    //		board = board.rotate();
    //
    //		trainingData.add( new BoardTrainingData(board, play, score) );
    //		trainingData.add( new BoardTrainingData(board.mirrorX(), play.mirrorX(), score) );
    
    		
    //		return trainingData;
    	}
	
	
	def learnFromGame(currentGame:TicTacToeGame, player:Mark, reward:Double ) : Set[BoardTrainingData] =  {
		
		if(currentGame.history != null){

		  val lastHistory = currentGame.history
			
			val lastPlay = lastHistory.play
			val lastGameState = lastHistory.game
			val lastBoard = lastGameState.board
			
			val previousHistory = learnFromGame(lastGameState, player, reward)
			
			if(lastPlay.player == player) {
  			trainCombinations(lastBoard, lastPlay, reward) ++  previousHistory
			}
			else{
			  previousHistory
			}
			
		}
		else{
		  Set()		  
		}
	}
}


class MatchMaker(val ai:TicTacToeAI)  {

	val logger = LoggerFactory.getLogger(classOf[MatchMaker]);

	var game = TicTacToeGame.startNewGame();

	def aiPlay() {
		if( !game.gameOver ){
			val aiPlay = ai.getPlay(game.board, game.getNextPlayer());
			playAndLearn(aiPlay.getBestResult());
		}
	}
	
	def back() {
		if(game.history != null){
			game = game.history.game
		}
	}
	
	
	def loadAi( fileName:String ) {
		ai.loadAI(fileName);
	}
	

	def tryLoadAi( fileName:String ) {
		logger.debug("loading knowledge... ");
		
		val trainingData = for (line <- Source.fromFile(fileName).getLines()) yield {
			
		  val values = line.split(",").map(  item => item.toDouble )

		  val score = values.last
		  
		  val features = values.take(values.length -1).toArray
		  
			new TrainingData(features, score);
		}
		
		ai.forget()
		ai.train(trainingData.toList)
	}
	

	def save(fileName:String) {
		logger.debug("saving knowledge... ");
		ai.saveAI(fileName)
	}

	
	def forget() {
			ai.forget()
	}
	
	def resetGame() {
		game = TicTacToeGame.startNewGame();
	}

	def play( x:Int, y:Int) {
		if(!game.gameOver){
			val play = new Play(x, y, game.getNextPlayer());
			playAndLearn(play);
		}
	}

	def playAndLearn( play:Play ) {
		game = game.play(play);
		
//		if(game.isGameOver()){
//			Set<BoardTrainingData> trainingData = learnGame(game);
//			ai.train(trainingData);
//			ai.fit();
//		}
	}

	
	def train( trainingData:Set[BoardTrainingData] ) {
		for(data <- trainingData){
			ai.learn(data.board, data.play, data.score)
		}
	}

	def teach( list : List[BoardTrainingData] ) {
		ai.learn(list);
	}
	
}
