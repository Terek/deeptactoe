package hu.terek.deeptactoe;

class BoardTrainingData(board:TicTacToeBoard , play:Play, val score:Double) extends PlayFeatures(board, play) {

	override def toString() : String = {
		"BoardTrainingData [board=" + board + ", play=" + play + ", score=" + score + "]";
	}

	override def canEqual(other: Any): Boolean = other.isInstanceOf[BoardTrainingData]

	override def equals(other: Any): Boolean = other match {
		case that: BoardTrainingData =>
			super.equals(that) &&
				(that canEqual this) &&
				score == that.score
		case _ => false
	}

	override def hashCode(): Int = {
		val state = Seq(super.hashCode(), score)
		state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
