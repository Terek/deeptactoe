package hu.terek.deeptactoe;

import org.junit.Test;

import hu.terek.deeptactoe.ai.DumbAI;
import hu.terek.deeptactoe.gui.MatchMaker;
import org.scalatest.FunSuite

class MatchMakerTest extends FunSuite {

	
  test("wrong move") {
		
		var game = TicTacToeGame.startNewGame();
		
		game = game.play( new Play(0, 0, game.getNextPlayer()) );
		game = game.play( new Play(0, 2, game.getNextPlayer()) );
		game = game.play( new Play(2, 0, game.getNextPlayer()) );
		game = game.play( new Play(0, 0, game.getNextPlayer()) );
		
		MatchMaker.learnGame(game);
	}

	
	test("winner moves") {
		
		val matchMaker = new MatchMaker(new DumbAI());
		
		var game = TicTacToeGame.startNewGame();
		
		game = game.play( new Play(0, 0, game.getNextPlayer()) );
		game = game.play( new Play(1, 2, game.getNextPlayer()) );
		game = game.play( new Play(0, 1, game.getNextPlayer()) );
		game = game.play( new Play(2, 2, game.getNextPlayer()) );
		game = game.play( new Play(0, 2, game.getNextPlayer()) );
		
		MatchMaker.learnGame(game);
	}
	
	
	test("draw"){
		val matchMaker = new MatchMaker(new DumbAI());
		
		var game = TicTacToeGame.startNewGame();
		
		game = game.play( new Play(0, 0, game.getNextPlayer()) );
		game = game.play( new Play(1, 2, game.getNextPlayer()) );
		game = game.play( new Play(0, 1, game.getNextPlayer()) );
		game = game.play( new Play(2, 2, game.getNextPlayer()) );
		game = game.play( new Play(0, 2, game.getNextPlayer()) );
		
		MatchMaker.learnGame(game);
	}
	
}
