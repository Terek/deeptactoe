package hu.terek.deeptactoe.ai;

import org.junit.Test;

import hu.terek.deeptactoe.Mark;
import hu.terek.deeptactoe.Play;
import hu.terek.deeptactoe.TicTacToeBoard;
import org.scalatest.FunSuite

class NeuralNetAiTest extends FunSuite {
	
	
	test(" test "){

	  val ai : NeuralNetAI = new NeuralNetAI();
		
		var board = new TicTacToeBoard();
		board = board.set(0, 0, Mark.X);
		
		val play : Play = new Play(0,0,Mark.O);
		
		ai.learn(board, play, 1)
		
	}

}
