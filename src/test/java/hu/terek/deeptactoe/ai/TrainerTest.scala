package hu.terek.deeptactoe.ai

import java.io.{File, FileOutputStream, PrintWriter}

import hu.terek.deeptactoe.TicTacToeBoard.getAllPlays
import hu.terek.deeptactoe._
import hu.terek.deeptactoe.gui.MatchMaker
import org.scalatest.{BeforeAndAfter, FunSuite}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.immutable.List
import scala.io.Source
import scala.util.Random

class TrainerTest extends FunSuite with BeforeAndAfter{
	
	val BATCH_SIZE : Int = 50;
	private val logger : Logger  = LoggerFactory.getLogger(classOf[TrainerTest]);

	val random : Random = new Random()
	
	
	before {

	}
	
	def generateAllPossibleGames( game : TicTacToeGame ) : (List[TicTacToeGame], List[BoardTrainingData]) = {

		def canWinButDoesNot(play: Play) = {
			val possibleWinnerPlaysForPlayer = game.board.getPossibleWinnerPlays(game.getNextPlayer())
			!possibleWinnerPlaysForPlayer.isEmpty && !possibleWinnerPlaysForPlayer.contains(play)
		}

		def shouldDefendButDoesNot(play: Play) = {
			val possibleWinnerPlaysForEnemy = game.board.getPossibleWinnerPlays( game.getNextPlayer().other )

			val canEnemyWin = !possibleWinnerPlaysForEnemy.isEmpty
			val playerDoesNotDefend = !possibleWinnerPlaysForEnemy.find( winner => winner.getIndex() == play.getIndex()).nonEmpty

			canEnemyWin && playerDoesNotDefend
		}

		if ( game.gameOver ) {
			(List(game), List())
		}
		else{
		  val results = for ( play <- getAllPlays(game.getNextPlayer()).toList )  yield {

				var (gameData, trainingData) = generateAllPossibleGames(game.play(play))

				if( canWinButDoesNot( play ) ) {
					(gameData , new BoardTrainingData( game.board, play,  TicTacToeAI.PUNISHMENT_FOR_LOSS) :: trainingData)
				}
				else if ( shouldDefendButDoesNot( play )){
					(gameData , new BoardTrainingData( game.board, play,  TicTacToeAI.PUNISHMENT_FOR_LOSS) :: trainingData)
				}
				else{
					(gameData, trainingData)
				}

			}

			val gameData = results.map( _._1 ).flatten
			val boardTrainingData = results.map(_._2).flatten

			(gameData, boardTrainingData)
		}

	}



	def generateAllPossibleGames() : List[BoardTrainingData] = {
		logger.debug("generating games")

		val (allPossibleGames, generatedBoardTrainingData) = generateAllPossibleGames( TicTacToeGame.startNewGame() );
		val trainingDataFromGames = MatchMaker.createBoardTrainingData(allPossibleGames);
		

		logger.debug("generated games: " + allPossibleGames.size);
		logger.debug("generated raw data: " + generatedBoardTrainingData.size);
		logger.debug("generated training data from games: " + trainingDataFromGames.size);

		return trainingDataFromGames ++ generatedBoardTrainingData;
	}
	


	def generateOrLoadTrainingData( ) : List[BoardTrainingData] = {
		val fileName : String = "boardTrainingData.txt"

		val file : File = new File(fileName)
		
		if( !file.exists() ){
			val allTrainingDataSet = generateAllPossibleGames()
			
			save(allTrainingDataSet, fileName)
			
			allTrainingDataSet
		}
		else {
			return load(fileName);
		}
		
		
	}
	
	def processTrainingData( allTrainingDataSet : List[BoardTrainingData] ) : List[BoardTrainingData] = {
		logger.debug("full training data set size: " + allTrainingDataSet.size)

		
		val groupedResults = allTrainingDataSet.groupBy( item => new PlayFeatures(item.board, item.play) )

		
		
		val processedTrainingData = for ( (grouped, listOfTrainingData) <- groupedResults ) yield {
			val avgScore = listOfTrainingData.map( item => item.score ).sum / listOfTrainingData.size

			new BoardTrainingData(grouped.board, grouped.play, avgScore );
		}
		
		processedTrainingData.toList
	}
	
	
	
	def  getSquaredError( ai : NeuralNetAI, list : List[BoardTrainingData] ) : Double = {
		
		val squaredErrors : List[Double] = for( data <- list) yield {
			val actualScore = ai.getPlayScore(data.board, data.play)
			(actualScore - data.score) * (actualScore - data.score);
		}
		
		return squaredErrors.sum / list.size;
	}
	

	def testWinRate(iterations:Int, ai : TicTacToeAI, dumbAi : TicTacToeAI, exploration : Boolean) : List[TicTacToeGame] = {

		val aiMap : Map[Mark, TicTacToeAI] = Map(Mark.X -> ai, Mark.O -> dumbAi)

		val playedGames = for( i <- 0 until iterations ) yield {

			var game : TicTacToeGame =  TicTacToeGame.startNewGame()
			
			while( !game.gameOver ){
				val next = game.getNextPlayer()

				val nextMove = aiMap(next).getPlay(game.board, next)

				game = game.play( nextMove.getBestResult() )
			}
			
			game
			
		}
		
		logger.debug("All games: " + playedGames.size );
		logger.debug("Current win count: " + playedGames.count( _.winner == Mark.X ));
		logger.debug("Current lost count: " + playedGames.count( _.winner == Mark.O ));

		playedGames.toList
	}
	
	def save(allTrainingDataSet : List[BoardTrainingData] , fileName : String ) {
		logger.debug("saving training data... ")
		
		val file = new File(fileName)  
		
		val outputStream : FileOutputStream = new FileOutputStream( file )
		
		val writer : PrintWriter = new PrintWriter( outputStream)
		
		for(boardTrainingData <- allTrainingDataSet){
			writer.println( SerializerUtilities.toJson(boardTrainingData) );
		}
		
		writer.close()
	}
	
	
	def load( fileName : String ) : List[BoardTrainingData] = {
		logger.debug("Loading training data... ")
		
		val file = new File(fileName)  
		
		val trainingData = for (line <- Source.fromFile(fileName).getLines()) yield {
		  SerializerUtilities.fromJson(line, classOf[BoardTrainingData])
		}
		
		trainingData.toList
		
	}
	
	test("learning with generated data") {
		
		val dumbAi : TicTacToeAI = new DumbAI();
		val ai : NeuralNetAI = new NeuralNetAI();
		
		val matchMaker : MatchMaker = new MatchMaker(ai);

		val allTrainingDataSet = generateOrLoadTrainingData();

		val processedTrainingData = processTrainingData(allTrainingDataSet);
		save(processedTrainingData, "processedBoardTrainingData.txt");

		for( i <-  0 to 10 ) {
			println ("iteration : " + i)
			learnFromData(i, ai, processedTrainingData)

			println ("iteration : " + i + " copmpleted")
		}


		println("final win rate test :")
		testWinRate(1000, ai, dumbAi, false);


		ai.saveAI("savedNET.zip");
		// matchMaker.save("trainingData.txt");
		
	}


	private def learnFromData(iteration : Int, ai: NeuralNetAI, processedTrainingData: List[BoardTrainingData]) = {
		val shuffledTrainingData = random.shuffle(processedTrainingData);

		// logger.debug("squaredError on all training data: " + getSquaredError(ai, processedTrainingData));

		val partitions = shuffledTrainingData.grouped(100).toList

		var partition: Int = 0

		for (sublist <- partitions) {

			ai.learn(sublist);

			partition += 1

			val sublistIteration = partitions.size * iteration + partition

			logger.debug("taught partition: " + sublistIteration + " of " + partitions.size);

		}

	}

	test(" dumb test "){
		
		val dumbAi : TicTacToeAI = new DumbAI()
		val ai : TicTacToeAI = new CachingAI()

		for(i <- 0 to 10000){
      		ai.learn(MatchMaker.createBoardTrainingData(testWinRate(100, ai, dumbAi, false))) 
      		ai.learn(MatchMaker.createBoardTrainingData(testWinRate(100, ai, dumbAi, true))) 

    			logger.debug("trained: " + i)
		}
	}
	
}
