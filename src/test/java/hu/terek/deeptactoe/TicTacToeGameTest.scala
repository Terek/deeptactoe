package hu.terek.deeptactoe

import hu.terek.deeptactoe.Mark._

import org.junit.Before
import org.junit.Test
import org.scalatest.BeforeAndAfter
import org.scalatest.FunSuite
import java.util.Arrays

class TicTacToeGameTest extends FunSuite with BeforeAndAfter {

	var game:TicTacToeGame = TicTacToeGame.startNewGame()

	def isX(mark:Mark) = mark == X
	def isO(mark:Mark) = mark == O
	
	before {
		game = TicTacToeGame.startNewGame
	}
	
	test("play one x"){
		val play = new Play(0,0,X)
		
		game = game.play(play)

		assert( game.board.state(0) == X )
		assert( game.board.get(0,0) == X )
		
		assert( game.board.state.count( isX ) == 1 )

		assert( ! game.gameOver)
		assert( game.winner == Empty )
		assert( game.winningMoves.isEmpty )
	}
	
	test("play two times"){
		val play = new Play(0,0,X)

		game = game.play(play)
		game = game.play(play)
		
		assert( game.board.state(0) == X )
		assert( game.board.get(0,0) == X )
		
		assert( game.board.state.count( isX ) == 1 )
		
		assert( game.gameOver)
		assert( game.winner == O )
		assert( game.winningMoves == (Set(play)))
	}
	
}
