package hu.terek.deeptactoe

import hu.terek.deeptactoe.Mark._

import java.util.function.Predicate

import org.hamcrest.Matchers
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.scalatest.FunSuite
import org.scalatest.BeforeAndAfter
import scala.collection.immutable.Set

class TicTacToeBoardTest extends FunSuite with BeforeAndAfter{

	var board:TicTacToeBoard = new TicTacToeBoard()

	def isX(mark:Mark) = mark == X
	def isO(mark:Mark) = mark == O

	before {
		board = new TicTacToeBoard()
	}

	test("Empty board"){
		assert( board.state != null )
		assert( board.state.length == 9)		
		assert( board.state.forall ( item => item == Empty ) )		
	}
	
	
	test("set one X"){
		board = board.set(0,0, X)

		assert( board.state(0) == X )
		assert( board.get(0,0) == X )
		
		assert( board.state.count(isX) == 1 )
	}
	
	test("set one O"){
		board = board.set(0,0, O)
		
		assert( board.state(0) == O )
		assert( board.get(0,0) == O )
		
		assert( board.state.count( isO ) == 1 )
	}
	
	
	test("set One X One O"){
		board = board.set(0,0, X)
		board = board.set(1,0, O)

		assert(board.get(0,0) == X)
		assert(board.get(1,0) == O)

		assert(board.state(0) == X )
		assert(board.state(1) == O )

		assert( board.state.count( isO ) == 1 )
		assert( board.state.count( isX ) == 1 )
	}

	
	test("set X At Location") {
		for(i <- 0 to 2; j <- 0 to 2){
				board = new TicTacToeBoard()
				board = board.set(i, j, X)
				
				assert(board.state( i + 3*j ) == X )
				assert(board.get(i,j) == X )
				assert(board.state.count( isX ) == 1 )
		}
	}
	
	
	test("get win field combinations"){
		val winFieldCombinations = TicTacToeBoard.getWinFieldCombinations()
		
		assert(winFieldCombinations.contains( Set(0,1,2) ))
		assert(winFieldCombinations.contains( Set(3,4,5) ))
		assert(winFieldCombinations.contains( Set(6,7,8) ))

		assert(winFieldCombinations.contains( Set(0,3,6) ))
		assert(winFieldCombinations.contains( Set(1,4,7) ))
		assert(winFieldCombinations.contains( Set(2,5,8) ))

		assert(winFieldCombinations.contains( Set(0,4,8) ))
		assert(winFieldCombinations.contains( Set(2,4,6) ))
	}

	test("get column and row test"){
		
		assert(TicTacToeBoard.getColumn(0) == 0)
		assert(TicTacToeBoard.getColumn(1) == 1)
		assert(TicTacToeBoard.getColumn(2) == 2)
		
		assert(TicTacToeBoard.getColumn(3) == 0)
		assert(TicTacToeBoard.getColumn(4) == 1)
		assert(TicTacToeBoard.getColumn(5) == 2)
		
		assert(TicTacToeBoard.getColumn(6) == 0)
		assert(TicTacToeBoard.getColumn(7) == 1)
		assert(TicTacToeBoard.getColumn(8) == 2)
		
		assert(TicTacToeBoard.getRow(0) == 0)
		assert(TicTacToeBoard.getRow(1) == 0)
		assert(TicTacToeBoard.getRow(2) == 0)
		
		assert(TicTacToeBoard.getRow(3) == 1)
		assert(TicTacToeBoard.getRow(4) == 1)
		assert(TicTacToeBoard.getRow(5) == 1)
		
		assert(TicTacToeBoard.getRow(6) == 2)
		assert(TicTacToeBoard.getRow(7) == 2)
		assert(TicTacToeBoard.getRow(8) == 2)
	}
	
	test("get possible winners test"){

		validateWinningPlay(new Play(0,0,X),new Play(0,1,X),new Play(0,2,X))
		validateWinningPlay(new Play(1,0,X),new Play(1,1,X),new Play(1,2,X))
		validateWinningPlay(new Play(2,0,X),new Play(2,1,X),new Play(2,2,X))

		validateWinningPlay(new Play(0,0,X),new Play(1,0,X),new Play(2,0,X))
		validateWinningPlay(new Play(0,1,X),new Play(1,1,X),new Play(2,1,X))
		validateWinningPlay(new Play(0,2,X),new Play(1,2,X),new Play(2,2,X))

		validateWinningPlay(new Play(0,0,X),new Play(1,1,X),new Play(2,2,X))
		validateWinningPlay(new Play(0,2,X),new Play(1,1,X),new Play(2,0,X))


		validateWinningPlay(new Play(0,0,O),new Play(0,1,O),new Play(0,2,O))
		validateWinningPlay(new Play(1,0,O),new Play(1,1,O),new Play(1,2,O))
		validateWinningPlay(new Play(2,0,O),new Play(2,1,O),new Play(2,2,O))

		validateWinningPlay(new Play(0,0,O),new Play(1,0,O),new Play(2,0,O))
		validateWinningPlay(new Play(0,1,O),new Play(1,1,O),new Play(2,1,O))
		validateWinningPlay(new Play(0,2,O),new Play(1,2,O),new Play(2,2,O))

		validateWinningPlay(new Play(0,0,O),new Play(1,1,O),new Play(2,2,O))
		validateWinningPlay(new Play(0,2,O),new Play(1,1,O),new Play(2,0,O))

	}

	def validateWinningPlay(expectedWinningPlaysArray: Play*) {
		val expectedWinningPlays = expectedWinningPlaysArray.toSet
		
		var board = new TicTacToeBoard()
		for(play <- expectedWinningPlays){
			board = board.set(play.x, play.y, play.player)
		}

		assert(board.getWinnerPlays() == expectedWinningPlays)
	}
	
	
	test("get possible winners test no win"){
		validateNoWinningPlay(new Play(0,0,O),new Play(0,1,X),new Play(0,2,X))
	}

	def validateNoWinningPlay(plays : Play*) {
		var board = new TicTacToeBoard()
		for(play <- plays){
			board = board.set(play.x, play.y, play.player)
		}

		assert(board.getWinnerPlays().isEmpty)
	}

	test("rotation with Play"){
		assert(new Play(0, 0, Empty).rotate() == new Play(2,0, Empty))
		assert(new Play(1, 0, Empty).rotate() == new Play(2,1, Empty))
		assert(new Play(2, 0, Empty).rotate() == new Play(2,2, Empty))

		assert(new Play(0, 1, Empty).rotate() == new Play(1,0, Empty))
		assert(new Play(1, 1, Empty).rotate() == new Play(1,1, Empty))
		assert(new Play(2, 1, Empty).rotate() == new Play(1,2, Empty))
		
		assert(new Play(0, 2, Empty).rotate() == new Play(0,0, Empty))
		assert(new Play(1, 2, Empty).rotate() == new Play(0,1, Empty))
		assert(new Play(2, 2, Empty).rotate() == new Play(0,2, Empty))
	}
	
	test("board rotation"){
		assert(getSimpleBoard(0, 0).rotate() ==  getSimpleBoard(2, 0) )
		assert(getSimpleBoard(1, 0).rotate() ==  getSimpleBoard(2, 1) )
		assert(getSimpleBoard(2, 0).rotate() ==  getSimpleBoard(2, 2) )
		
		assert(getSimpleBoard(0, 1).rotate() ==  getSimpleBoard(1, 0) )
		assert(getSimpleBoard(1, 1).rotate() ==  getSimpleBoard(1, 1) )
		assert(getSimpleBoard(2, 1).rotate() ==  getSimpleBoard(1, 2) )
		
		assert(getSimpleBoard(0, 2).rotate() ==  getSimpleBoard(0, 0) )
		assert(getSimpleBoard(1, 2).rotate() ==  getSimpleBoard(0, 1) )
		assert(getSimpleBoard(2, 2).rotate() ==  getSimpleBoard(0, 2) )
	}

	def getSimpleBoard(i:Int, j:Int) = new TicTacToeBoard().set(i, j, O)


	test("getPossibleWinnerPlays") {

		var board = new TicTacToeBoard()
			.set(1, 1, X)
			.set(0, 2, O)
			.set(2, 2, X)
			.set(2, 0, O)

		println(board)

		assert( board.getPossibleWinnerPlays( X ) == Set(new Play(0,0,X)))


	}

	
}
