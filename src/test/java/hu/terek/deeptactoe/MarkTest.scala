package hu.terek.deeptactoe

import org.scalatest.FunSuite

/**
  * Created by terek on 2017. 06. 30..
  */
class MarkTest extends FunSuite {


    test("Mark to string"){
      assert (Mark.X.toString == "X" )
      assert (Mark.O.toString == "O" )
      assert (Mark.Empty.toString == "Empty" )
    }

  test("Mark values"){

    assert( Mark.fromString("X") == Mark.X)
    assert( Mark.fromString("O") == Mark.O)
    assert( Mark.fromString("Empty") == Mark.Empty)

  }



}
